import Vue from 'vue'
import VueRouter from 'vue-router'
import App from './App.vue'
import editMovie from './components/editMovie'
import addMovie from './components/addMovie'
import displayMovies from './components/displayMovies'
import vuetify from './plugins/vuetify';
import Movie from './classes/movie';


Vue.config.productionTip = false;

Vue.use(VueRouter);

const axios = require('axios').default;
axios.defaults.baseURL = 'http://127.0.0.1:3000';
window.axios = require('axios');

const routes = [
    { path: '/movie/:index/edit', component:editMovie, name:"editMovie" },
    { path: '/movie/add', component:addMovie, name:"addMovie" },
    { path: '/', component:displayMovies }
  ]

const router = new VueRouter({
routes
})

window.movies = [];

let mov1 = new Movie(0, 'Test', 2000, 'Français', 'Tony', 'Action', 'https://cdn.vuetifyjs.com/images/cards/sunshine.jpg', 'Aifgrze,hguirehgureigh, gregui hger ,gregu,ier hjuierg huig ,er gergjhu erg,jher g,hug. Ajuerigjheruiog, gruei,g jhgurie, jguireg,j rie', 4);
let mov2 = new Movie(1, 'Tesgsdfggfdgt', 2000, 'Français', 'GGGDGZ', 'Action', 'https://cdn.vuetifyjs.com/images/cards/sunshine.jpg', 'Aifgrze,hguirehgureigh, gregui hger ,gregu,ier hjuierg huig ,er gergjhu erg,jher g,hug. Ajuerigjheruiog, gruei,g jhgurie, jguireg,j rie', 4);
window.movies.push(mov1);
window.movies.push(mov2);


new Vue({
    render: h => h(App),
    vuetify,
    router
}).$mount('#app')




